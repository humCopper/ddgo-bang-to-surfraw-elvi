#!/usr/bin/env python3
import os
from string import Template
import sys
import urllib.request


# Find bang from duckduckgo
def find_bang(bang_name, bangs):
    bang = next((bang for bang in bangs if bang['t'] == bang_name), False)
    bang['u'] = bang['u'].replace('{{{s}}}', '')
    return bang

# generate elvi file from bang
def generate_elvi(bang):
    template_file = "elvi_template"
    result = ''
    elvi_path = os.path.expanduser("~/.config/surfraw/elvi/")

    with open(template_file, 'r') as temp_file:
        src = Template(temp_file.read())
        result = src.safe_substitute(bang)

    if not os.path.exists(elvi_path):
        os.makedirs(elvi_path)

    with open(elvi_path + bang['t'], 'w') as elvi_file:
        elvi_file.write(result);
    os.chmod(elvi_path + bang['t'], 0o755)


def main(argv):
    url = "https://duckduckgo.com/bang.js"
    file_name = "bang.py"

    # Start Dictionary
    with open(file_name, 'w') as out_file:
        out_file.write("bangs = ")

    # Download the file from `url` and save it locally under `file_name`:
    # Download bangs and make it a python dictionary
    with urllib.request.urlopen(url) as response, open(file_name, 'ab') as out_file:
        data = response.read() # a `bytes` object
        out_file.write(data)

    bangs = __import__(file_name[:4]).bangs

    if len(argv) != 0:
        bang = find_bang(argv[0], bangs)
        generate_elvi(bang)
    else:
        print("Error: No Arguments")
    os.remove("bang.py")
if __name__ == "__main__":
    main(sys.argv[1:])
